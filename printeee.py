import schedule
import time
import shutil
import os
import subprocess

bluetooth_address = "" #ie "00:1F:47:37:7C:02"
cmd = "! obexftp --nopath --noconn --uuid none --bluethooth {0} --channel 4 -p {1} | grep -s \"fail\""
source_dir = "./source"
destination_dir = "./destination"

def proccess(file):
    try:
        print("sending " + file)
        subprocess.check_output(cmd.format(bluetooth_address, file), shell=True, stderr=subprocess.STDOUT)
        shutil.move(source_dir + "/" + file, destination_dir + "/" + file)
        pass
    except Exception as e:
        raise

def job():
    files = next(os.walk(source_dir))[2]
    files.sort(key=lambda file: os.stat(source_dir + "/" + file).st_mtime)
    proccess(files[0]) if files else None

schedule.every(5).seconds.do(job)

while True:
    schedule.run_pending()
    time.sleep(1)
